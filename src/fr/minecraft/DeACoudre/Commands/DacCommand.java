package fr.minecraft.DeACoudre.Commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import Commands.MinecraftCommand;
import fr.minecraft.DeACoudre.Controllers.GameInstanceController;
import fr.minecraft.DeACoudre.Models.CustomException;
import fr.minecraft.DeACoudre.TabCompleters.DacTabCompleter;

public class DacCommand extends MinecraftCommand
{
	static {
		COMMAND_NAME = "dac";
		TAB_COMPLETER = DacTabCompleter.class;
	}
	
	/**
	 * dac [join | leave | start | leaderboard]
	 * */
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if(sender instanceof Player)
		{
			Player player = (Player) sender;
			if(args.length==1)
			{
				switch(args[0].toUpperCase())
				{
				case "JOIN":
					this.onJoin(player);
					break;
				case "LEAVE":
					this.onLeave(player);
					break;
				case "START":
					this.onStart(player);
					break;
				case "LEADERBOARD":
					this.onLeaderBoard(player);
					break;
				default:
					player.sendMessage(ChatColor.RED+"Commande invalide!");
					break;
				}
			}
			else
				player.sendMessage(ChatColor.RED+"Commande invalide!");
		}
		else
			sender.sendMessage(ChatColor.RED+"Commande impossible pour un non-joueur!");
		return true;
	}

	private void onLeaderBoard(Player player)
	{
		player.sendMessage(ChatColor.RED+"Not implemented!");
	}

	private void onStart(Player player)
	{
		try {
			GameInstanceController.start(player);
		} catch (CustomException e) {
			player.sendMessage(ChatColor.RED+e.getMessage());
		}
	}

	private void onLeave(Player player)
	{
		try {
			GameInstanceController.leave(player);
		} catch (CustomException e) {
			player.sendMessage(ChatColor.RED+e.getMessage());
		}
	}

	private void onJoin(Player player)
	{
		try {
			GameInstanceController.join(player);
		} catch (CustomException e) {
			player.sendMessage(ChatColor.RED+e.getMessage());
		}
	}

}
