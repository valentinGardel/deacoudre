package fr.minecraft.DeACoudre.Commands;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import Commands.MinecraftCommand;
import fr.minecraft.DeACoudre.ConfigDAC;
import fr.minecraft.DeACoudre.TabCompleters.DacSettingsTabCompleter;

public class ParametrageDAC extends MinecraftCommand
{
	static {
		COMMAND_NAME = "setdac";
		TAB_COMPLETER = DacSettingsTabCompleter.class;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if(sender instanceof Player) {
			Player player = (Player) sender;
			if(args.length == 1) {
				
				switch(args[0].toUpperCase())
				{
				case "SPECTATOR-LOCATION":
					ConfigDAC.GetConfig().setLocSpec(player.getLocation());
					player.sendMessage(ChatColor.GREEN+"Position spectateur d�fini!");
					break;
				case "PLAYER-LOCATION":
					ConfigDAC.GetConfig().setLocPlay(player.getLocation());
					player.sendMessage(ChatColor.GREEN+"Position de jeu d�fini!");
					break;
				case "AFK-LOCATION":
					ConfigDAC.GetConfig().setLocaForcePlay(player.getLocation());
					player.sendMessage(ChatColor.GREEN+"Position afk d�fini!");
					break;
				case "POOL-START":
					ConfigDAC.GetConfig().setPoolStart(player.getLocation());
					player.sendMessage(ChatColor.GREEN+"Position piscine debut d�fini");
					break;
				case "POOL-END":
					ConfigDAC.GetConfig().setPoolEnd(player.getLocation());
					player.sendMessage(ChatColor.GREEN+"Position piscine fin d�fini");
					break;
				default:
					player.sendMessage(ChatColor.RED+"Commande invalide!");
					break;
				}
			}
			else if(args.length == 2) {
				
				int value;
				try {
					value=Integer.parseInt(args[1]);
				}catch(Exception e) {
					player.sendMessage(ChatColor.RED+"Valeur invalide!");
					return true;
				}
				switch(args[0].toUpperCase())
				{
				case "AFK-TIME":
					ConfigDAC.GetConfig().setTimeAFK(value);
					player.sendMessage(ChatColor.GREEN+"temps AFK chang�");
					break;
				case "DEFAULT-HEALTH":
					ConfigDAC.GetConfig().setDefaultHealth(value);
					player.sendMessage(ChatColor.GREEN+"Vie par defaut chang�!");
					break;
				case "MINIMUM-PLAYER":
					ConfigDAC.GetConfig().setMinimumPlayer(value);
					player.sendMessage(ChatColor.GREEN+"Minimim de joueur chang�!");
					break;
				default:
					player.sendMessage(ChatColor.RED+"Commande invalide!");
					break;
				}
			}
		}
		else
			sender.sendMessage(ChatColor.RED+"Impossible pour un non-joueur!");
		return true;
	}

}
