package fr.minecraft.DeACoudre.Controllers;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

import Commons.MessageBuilder;
import fr.minecraft.DeACoudre.ConfigDAC;
import fr.minecraft.DeACoudre.Main;
import fr.minecraft.DeACoudre.Models.CustomException;
import fr.minecraft.DeACoudre.Models.GameInstance;
import fr.minecraft.DeACoudre.Models.PlayerDAC;
import fr.minecraft.DeACoudre.Models.Result;

public class GameInstanceController {
	private final static String SUFFIX = ChatColor.GOLD+"[DAC] ";
	private static BukkitTask afkDetection;
	
	public static void join(Player sender) throws CustomException
	{
		GameInstance instance = GameInstance.getInstance();
		if(instance==null)
		{
			instance = GameInstance.createGame(sender);
			MessageBuilder.create(SUFFIX+sender.getName()+" a cr�� une partie! ")
				.onHoverText("Rejoindre").onClickRunCommand("/dac join").write(ChatColor.GREEN+"[REJOINDRE]")
				.broadcast();
		}
		else
		{
			if(instance.getPlayerDAC(sender)!=null) throw new CustomException("Vous �tes d�j� dans la partie!");
			GameInstanceController.announce(sender.getName(), "rejoint la partie");
			sender.sendMessage(SUFFIX+"Vous rejoignez la partie!");
		}
		if(instance.started)
			throw new CustomException("La partie a d�j� commenc�!");
		else
			instance.join(sender);
	}
	
	public static void leave(Player sender) throws CustomException
	{
		GameInstance instance = GameInstance.getInstance();
		if(instance==null)
			throw new CustomException("Aucune partie en cours!");
		else
		{
			PlayerDAC player = instance.getPlayerDAC(sender);
			if(player!=null)
			{
				if(instance.started)
				{
					instance.kill(player);
					instance.currentPlayer.isPlayer(sender);
					GameInstanceController.nextTurn(Result.FAILED, null);
				}
				else
					instance.leave(player);
			}
			else
				throw new CustomException("Vous n'�tes pas dans la partie!");
		}
	}
	
	public static void start(Player sender) throws CustomException
	{
		GameInstance instance = GameInstance.getInstance();
		if(instance==null)throw new CustomException("Aucune partie en cr�ation!");
		if(instance.started)throw new CustomException("Une partie est d�j� en cours!");
		if(instance.creator!=sender)throw new CustomException("Vous ne pouvez pas lancer, vous n'�tes pas le cr�ateur de la partie!");
		if(instance.players.size()<ConfigDAC.GetConfig().getMinimumPlayer())throw new CustomException("Vous ne pouvez pas lancer, pas assez de joueur!");
		instance.shufflePlayers();
		GameInstanceController.distributeMaterials();
		GameInstanceController.clearPool();
		Main.setListener();
		instance.start();
		GameInstanceController.announce("La partie d�marre", null);
		for(PlayerDAC player:instance.players)
			player.teleport(ConfigDAC.GetConfig().getLocSpec());
		PlayerDAC nextPlayer = instance.getNextPlayer();
		instance.setNextPlayer(nextPlayer);
		if(!nextPlayer.teleport(ConfigDAC.GetConfig().getLocPlay()))
			GameInstanceController.nextTurn(Result.FAILED, null);
		else
			GameInstanceController.timer(nextPlayer);
	}
	
	private static void distributeMaterials()
	{
		GameInstance instance = GameInstance.getInstance();
		for(int i=0;i<instance.players.size();i++)
		{
			instance.players.get(i).setMaterial(ConfigDAC.getMaterial(i));
		}
	}
	
	/* nettoit la piscine */
	public static void clearPool() {
		Location start = ConfigDAC.GetConfig().getPoolStart(), end = ConfigDAC.GetConfig().getPoolEnd();
		int x1, x2, y, z1, z2;
		if(start.getBlockX()<end.getBlockX())
		{
			x1=start.getBlockX();
			x2=end.getBlockX();
		}
		else
		{
			x2=start.getBlockX();
			x1=end.getBlockX();
		}
		if(start.getBlockZ()<end.getBlockZ())
		{
			z1=start.getBlockZ();
			z2=end.getBlockZ();
		}
		else
		{
			z2=start.getBlockZ();
			z1=end.getBlockZ();
		}
		y = start.getBlockY();
		World world = start.getWorld();
		for(int i=x1; i<=x2; i++) {
			for(int ii=z1; ii<=z2;ii++) {
				if(GameInstanceController.isMaterial(world.getBlockAt(i, y, ii).getType()))
					world.getBlockAt(i, y, ii).setType(Material.WATER);
			}
		}
	}
	
	private static boolean isMaterial(Material material)
	{
		for(Material m:ConfigDAC.materials)
		{
			if(m==material)
				return true;
		}
		return false;
	}
	
	public static void nextTurn(Result result, Location location)
	{
		GameInstanceController.cancelTimer();
		GameInstance instance = GameInstance.getInstance();
		switch(result)
		{
		case SUCCESS:
			location.getBlock().setType(instance.currentPlayer.getMaterial());
			break;
		case DAC:
			location.getBlock().setType(instance.currentPlayer.getMaterial());
			instance.currentPlayer.addHealth(1);
			break;
		case FAILED:
			instance.currentPlayer.removeHealth(1);
			break;
		}
		instance.currentPlayer.teleport(ConfigDAC.GetConfig().getLocSpec());
		if(instance.isFinished())
		{
			PlayerDAC winner = instance.getWinner();
			if(winner!=null)
				GameInstanceController.announceGameResult(winner);
			instance.destroy();
			Main.unregisterListener();
		}
		else {
			GameInstanceController.announceTurnResult(result);
			PlayerDAC nextPlayer = instance.getNextPlayer();
			instance.setNextPlayer(nextPlayer);
			if(!nextPlayer.teleport(ConfigDAC.GetConfig().getLocPlay()))
				GameInstanceController.nextTurn(Result.FAILED, null);
			else
				GameInstanceController.timer(nextPlayer);
		}
	}
	
	private static void cancelTimer(){
		if(GameInstanceController.afkDetection!=null)
			GameInstanceController.afkDetection.cancel();
	}
	
	private static void timer(PlayerDAC player) {
		GameInstanceController.afkDetection = Main.runTaskLater( new Runnable() {
			@Override
			public void run() {
//				player.sendTitle(ChatColor.RED+"Anti-AFK", null);
				player.teleport(ConfigDAC.GetConfig().getLocaForcePlayer());
			}
		}, ConfigDAC.GetConfig().getTimeAFK());
	}
	
	private static void announce(String title, String subtitle)
	{
		GameInstance instance = GameInstance.getInstance();
		for(PlayerDAC player:instance.players)
			player.sendTitle(title, subtitle);
	}
	
	private static void announceTurnResult(Result result)
	{
		GameInstance instance = GameInstance.getInstance();
		PlayerDAC currentPlayer = instance.currentPlayer;
		switch(result)
		{
		case SUCCESS:
			for(PlayerDAC player:instance.players)
			{
				if(player.equals(currentPlayer))
					player.sendTitle("Bravo", "vous avez reussi votre saut");
				else
					player.sendTitle(currentPlayer.getPlayerName(),"a reussi son saut");
			}
			break;
		case DAC:
			for(PlayerDAC player:instance.players)
			{
				if(player.equals(currentPlayer))
					player.sendTitle("Bravo", "vous avez fait un DAC");
				else
					player.sendTitle(currentPlayer.getPlayerName(), "a fait un DAC");
			}
			break;
		case FAILED:
			String status;
			if(currentPlayer.isAlive())
				status = (currentPlayer.getHealth()-1)+" vie(s) restante";
			else
				status = "Mort x_x";
			for(PlayerDAC player:instance.players)
			{
				if(player.equals(currentPlayer))
					player.sendTitle("Dommage", "vous avez rat� votre saut"+status);
				else
					player.sendTitle(currentPlayer.getPlayerName(), "a rat� son saut"+status);
			}
			break;
		}
	}
	
	private static void announceGameResult(PlayerDAC winner)
	{
		GameInstance instance = GameInstance.getInstance();
		for(PlayerDAC player:instance.players)
		{
			if(player.equals(winner))
				player.sendTitle("Bravo", "vous avez gagn� la partie");
			else
				player.sendTitle(winner.getPlayerName(), "a gagn� la partie");
		}
	}
}
