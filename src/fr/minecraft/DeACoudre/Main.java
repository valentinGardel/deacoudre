package fr.minecraft.DeACoudre;
import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.scheduler.BukkitTask;

import Commons.MinecraftPlugin;
import fr.minecraft.DeACoudre.Commands.*;
import fr.minecraft.DeACoudre.Listeners.*;

@SuppressWarnings("unchecked")
public class Main extends MinecraftPlugin
{
	static {
		PLUGIN_NAME = "MyHome";
		LOG_TOGGLE = false;
		COMMANDS = new Class[]{
				DacCommand.class,
				ParametrageDAC.class
			};
		LISTENERS = null;
		CONFIG = ConfigDAC.class;
	}
	
	private static Main INSTANCE;
	
	@Override
	public void onEnable()
	{
		super.onEnable();
		Main.INSTANCE = this;
	}

	public static void unregisterListener() {
		HandlerList.unregisterAll(Main.INSTANCE);
	}

	public static void setListener() {
		Main.INSTANCE.getServer().getPluginManager().registerEvents(new ListenerDAC(), Main.INSTANCE);
	}
	
	public static BukkitTask runTaskLater(Runnable runnable, int time)
	{
		BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
		return scheduler.runTaskLater(Main.INSTANCE, runnable, 20*time); 
	}

	@Override
	protected void initDatabase() throws Exception {}
}
