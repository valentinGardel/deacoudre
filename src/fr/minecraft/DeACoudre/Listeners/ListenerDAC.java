package fr.minecraft.DeACoudre.Listeners;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityToggleGlideEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;

import Commons.MinecraftListener;
import fr.minecraft.DeACoudre.ConfigDAC;
import fr.minecraft.DeACoudre.Controllers.GameInstanceController;
import fr.minecraft.DeACoudre.Models.GameInstance;
import fr.minecraft.DeACoudre.Models.Result;

@SuppressWarnings("unused")
public class ListenerDAC extends MinecraftListener
{

	/* detection tour gagne */
	@EventHandler
	public void onPlayerLandInWaterEvent(PlayerMoveEvent event) {
		/* on verifie que c'est le joueur dont c'est le tour */
		if(this.isCurrentPlayer(event.getPlayer())) {
			Location l = event.getFrom();
			
			if(l.getBlock().getType() == Material.WATER) {
//				l = this.roundLocation(l);
//				Bukkit.broadcast("x: "+l.getX()+", z: "+l.getZ(), "MyEssentials.DEBUG");
				Result result;
				if(!this.isInPool(l))
					result = Result.FAILED;
				else
					result = this.isDAC(l)?Result.DAC:Result.SUCCESS;
				GameInstanceController.nextTurn(result, l);
			}
		}
	}

	// 0.3<>0.7
	private Location roundLocation(Location l)
	{
		Location newLocation = l.clone();
		
		double x=l.getX()%1, z=l.getZ()%1;
		if(x<0)x=x*-1;
		if(z<0)z=z*-1;
//		Bukkit.broadcast("x: "+l.getX()+", z: "+l.getZ()+", modulo x: "+x+", modulo z: "+z, "MyEssentials.DEBUG");
		if(x<0.3)
			newLocation = newLocation.add(-1, 0, 0);
		else if(x>0.7)
			newLocation = newLocation.add(1, 0, 0);
		if(z<0.3)
			newLocation = newLocation.add(0, 0, -1);
		else if(z>0.7)
			newLocation = newLocation.add(0, 0, 1);
		return newLocation;
	}
	
	private boolean isCurrentPlayer(Player player)
	{
		return GameInstance.getInstance().currentPlayer.isPlayer(player);
	}
	
	private boolean isInPool(Location l) {
		Location start = ConfigDAC.GetConfig().getPoolStart(), end = ConfigDAC.GetConfig().getPoolEnd();
		return l.getWorld()==start.getWorld() && l.getBlockY()==start.getBlockY() && 
				this.isBetween(start.getBlockX(), end.getBlockX(), l.getBlockX()) && this.isBetween(start.getBlockZ(), end.getBlockZ(), l.getBlockZ());
	}
	
	private boolean isBetween(int a, int b, int c)
	{
		return (a<b)?a<=c && c<=b:a>=c && c>=b;
	}

	/* detect si le tour gagne est un dac */
	private boolean isDAC(Location l) {
		return !(this.locaIsWater(l.clone().subtract(1, 0, 0)) || this.locaIsWater(l.clone().add(1, 0, 0)) ||
				this.locaIsWater(l.clone().subtract(0, 0, 1)) || this.locaIsWater(l.clone().subtract(0, 0, 1)));
	}
	
	private boolean locaIsWater(Location l)
	{
		return l.getBlock().getType() == Material.WATER;
	}

	/* detection tour perdu */
	@EventHandler
	public void onFall(EntityDamageEvent event) {
		if(event.getEntity() instanceof Player && event.getCause() == DamageCause.FALL) {
			Player player = (Player) event.getEntity();
			if(this.isCurrentPlayer(player)) {
				event.setCancelled(true);
				GameInstanceController.nextTurn(Result.FAILED, player.getLocation());
			}
		}
	}

	/* desactive les commandes pendant le tour du joueur */
	@EventHandler
	public void onPlayerCommandPreprocessEvent(PlayerCommandPreprocessEvent event) {
		if(this.isCurrentPlayer(event.getPlayer())) {
			event.getPlayer().sendMessage(ChatColor.RED+"Vous devriez plutot jouer avant!");
			event.setCancelled(true);
		}
	}

	/* si le joueur deco pendant son tour, passe son tour */
	@EventHandler
	public void onPlayerQuitEvent(PlayerQuitEvent event) {
		if(this.isCurrentPlayer(event.getPlayer())) {
			GameInstanceController.nextTurn(Result.FAILED, null);
		}
	}

	/* desactive les enderpearl pendant le tour du joueur */
	@EventHandler
	public void onPlayerTeleportEvent(PlayerTeleportEvent event) {
		if((event.getCause() != TeleportCause.PLUGIN && event.getCause() != TeleportCause.UNKNOWN) && this.isCurrentPlayer(event.getPlayer())) {
			event.getPlayer().sendMessage(ChatColor.RED+"La triche c'est pas beau!");
			event.setCancelled(true);
		}
	}

	/* desactive les elytras */
	@EventHandler
	public void onEntityToggleGlideEvent(EntityToggleGlideEvent event) {
		if(event.getEntity() instanceof Player) {
			if(this.isCurrentPlayer((Player)event.getEntity())) {
				event.setCancelled(true);
				event.getEntity().sendMessage(ChatColor.RED+"La triche c'est pas beau!");
			}
		}
	}
}
