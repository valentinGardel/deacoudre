package fr.minecraft.DeACoudre.TabCompleters;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import Commands.MinecraftTabCompleter;

public class DacSettingsTabCompleter extends MinecraftTabCompleter
{

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args)
	{
		ArrayList<String> list = new ArrayList<String>();
		if(args.length==1)
		{
			list.add("SPECTATOR-LOCATION");
			list.add("PLAYER-LOCATION");
			list.add("AFK-LOCATION");
			list.add("AFK-TIME");
			list.add("DEFAULT-HEALTH");
			list.add("MINIMUM-PLAYER");
		}
		return list;
	}
}