package fr.minecraft.DeACoudre.TabCompleters;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import Commands.MinecraftTabCompleter;

public class DacTabCompleter extends MinecraftTabCompleter
{

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args)
	{
		ArrayList<String> list = new ArrayList<String>();
		list.add("JOIN");
		list.add("START");
		list.add("LEAVE");
		list.add("LEADERBOARD");
		return list;
	}
}
