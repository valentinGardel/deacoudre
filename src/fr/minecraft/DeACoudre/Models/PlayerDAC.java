package fr.minecraft.DeACoudre.Models;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public class PlayerDAC {
	private final static ChatColor SUFFIX_COLOR = ChatColor.GOLD;
	private Player player;
	private int health;
	private Material material;
	
	public PlayerDAC(Player player, int health)
	{
		this.player=player;
		this.health=health;
	}
	
	public void setMaterial(Material material) {
		this.material=material;
	}
	
	public Material getMaterial(){
		return this.material;
	}
	
	public boolean isAlive()
	{
		return this.health>0;
	}
	
	public boolean equals(Object o)
	{
		return o==this || (o instanceof PlayerDAC && ((PlayerDAC)o).player.equals(this.player));
	}

	public void sendTitle(String title, String subtitle) {
		if(this.player.isOnline())
		{
			if(title!=null)
				title=SUFFIX_COLOR+title;
			if(subtitle!=null)
				subtitle=SUFFIX_COLOR+subtitle;
			this.player.sendTitle(title, subtitle, 1, 20, 1);
		}
	}
	
	public void sendMessage(String message) {
		if(this.player.isOnline())
			this.player.sendMessage(SUFFIX_COLOR+message);
	}

	public boolean teleport(Location location) {
		if(this.player.isOnline())
			this.player.teleport(location);
		return this.player.isOnline();
	}

	public boolean isPlayer(Player player) {
		return this.player==player;
	}

	public void addHealth(int value) {
		this.health += value;
	}

	public void removeHealth(int value) {
		this.health -= value;
	}

	public String getPlayerName() {
		return this.player.getName();
	}

	public int getHealth() {
		return this.health;
	}
}
