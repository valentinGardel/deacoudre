package fr.minecraft.DeACoudre.Models;

public class CustomException extends Exception {
	public CustomException(String string) {
		super(string);
	}

	private static final long serialVersionUID = 5770197925247083670L;
}
