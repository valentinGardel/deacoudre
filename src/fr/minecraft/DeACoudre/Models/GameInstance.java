package fr.minecraft.DeACoudre.Models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.entity.Player;

import fr.minecraft.DeACoudre.ConfigDAC;

public class GameInstance
{
	private static GameInstance instance;
	
	public Player creator;
	public List<PlayerDAC> players;
	public PlayerDAC currentPlayer;
	public boolean started;
	
	private GameInstance(Player creator)
	{
		this.creator = creator;
		this.players = new ArrayList<PlayerDAC>();
		this.currentPlayer = null;
		this.started = false;
	}
	
	public static GameInstance getInstance()
	{
		return GameInstance.instance;
	}
	
	public static GameInstance createGame(Player creator)
	{
		GameInstance.instance=new GameInstance(creator);
		return GameInstance.instance;
	}
	
	public void destroy()
	{
		GameInstance.instance=null;
	}
	
	public void start()
	{
		this.started = true;
	}
	
	public void join(Player player)
	{
		this.players.add(new PlayerDAC(player, 
				ConfigDAC.GetConfig().getDefaultHealth()
			));
	}
	
	public boolean isFinished()
	{
		if(this.players.size()>1)
		{
			int playerAliveCount = 0;
			for(PlayerDAC player:this.players)
			{
				if(player.isAlive())
					playerAliveCount++;
			}
			return playerAliveCount<=1;
		}
		else if(this.players.size()==1)
			return !this.players.get(0).isAlive();
		else
			return true;
	}

	public PlayerDAC getWinner()
	{
		for(PlayerDAC player:this.players)
		{
			if(player.isAlive())
				return player;
		}
		return null;
	}

	public PlayerDAC getNextPlayer()
	{
		PlayerDAC nextPlayer=null;
		if(this.currentPlayer!=null)
		{
			int index = this.players.indexOf(this.currentPlayer);
			int playersCount = this.players.size();
			
			int i=(index+1)%playersCount;
			while(i!=index)
			{
				if(this.players.get(i).isAlive())
				{
					nextPlayer = this.players.get(i);
					break;
				}
				i=(i+1)%playersCount;
			}
			if(nextPlayer==null)
				nextPlayer = this.players.get(index);
		}
		else
			nextPlayer = this.players.get(0);
		return nextPlayer;
	}

	public void setNextPlayer(PlayerDAC nextPlayer)
	{
		this.currentPlayer=nextPlayer;
	}

	public PlayerDAC getPlayerDAC(Player sender) {
		for(PlayerDAC player:this.players)
		{
			if(player.isPlayer(sender))
				return player;
		}
		return null;
	}

	public void shufflePlayers() {
		Collections.shuffle(this.players);
	}

	public void kill(PlayerDAC player) {
		player.removeHealth(player.getHealth());
	}

	public void leave(PlayerDAC player) {
		this.players.remove(player);
	}
}
