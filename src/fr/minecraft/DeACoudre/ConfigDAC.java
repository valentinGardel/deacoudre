package fr.minecraft.DeACoudre;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.plugin.java.JavaPlugin;

import Commons.PluginConfig;

public class ConfigDAC extends PluginConfig
{
	public static Material[] materials = {
			Material.BLUE_WOOL, 
			Material.GREEN_WOOL, 
			Material.MAGENTA_WOOL, 
			Material.ORANGE_WOOL, 
			Material.WHITE_WOOL, 
			Material.YELLOW_WOOL, 
			Material.RED_WOOL, 
			Material.PURPLE_WOOL, 
			Material.PINK_WOOL, 
			Material.LIME_WOOL, 
			Material.LIGHT_BLUE_WOOL, 
			Material.LIGHT_GRAY_WOOL
		};
	
	private static ConfigDAC instance;
	
	public void initConfig(JavaPlugin plugin)
	{
		super.initConfig(plugin);
		ConfigDAC.instance = this;
	}
	
	public static ConfigDAC GetConfig()
	{
		return ConfigDAC.instance;
	}
	
	public void setDefaultHealth(int health)
	{
		this.set("default-health", health);
	}
	
	public int getDefaultHealth()
	{
		// Reading from the config
		return this.plugin.getConfig().getInt("default-health");
	}
	

	public void setLocSpec(Location location) {
		this.set("spectator-location", location);
	}

	public Location getLocSpec() {
		return this.plugin.getConfig().getObject("spectator-location", Location.class);
	}
	
	public void setLocPlay(Location location) {
		this.set("player-location", location);
	}

	public Location getLocPlay() {
		return this.plugin.getConfig().getObject("player-location", Location.class);
	}
	
	public void setPoolStart(Location location) {
		this.set("pool-start-location", location);
	}

	public Location getPoolStart() {
		return this.plugin.getConfig().getObject("pool-start-location", Location.class);
	}
	
	public void setPoolEnd(Location location) {
		this.set("pool-end-location", location);
	}

	public Location getPoolEnd() {
		return this.plugin.getConfig().getObject("pool-end-location", Location.class);
	}
	
	public void setLocaForcePlay(Location location) {
		this.set("afk-location", location);
	}
	
	public Location getLocaForcePlayer() {
		return this.plugin.getConfig().getObject("afk-location", Location.class);
	}

	public void setTimeAFK(int secondes) {
		this.set("afk-time", secondes);
	}
	
	public int getTimeAFK() {
		return this.plugin.getConfig().getInt("afk-time");
	}

	public void setMinimumPlayer(int minimum) {
		this.set("minimum-player", minimum);
	}
	
	public int getMinimumPlayer() {
		return this.plugin.getConfig().getInt("minimum-player");
	}

	public static Material getMaterial(int i) {
		return materials[i%materials.length];
	}
	
}
